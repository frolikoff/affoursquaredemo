//
//  AFDetailViewController.m
//  AFFoursquareDemo
//
//  Created by Alex Frolikoff on 2/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFDetailViewController.h"

@interface AFDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation AFDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:[[self pizzeria] name]];
    [[self addressLabel] setText:[[self pizzeria] address]];
    [[self phoneLabel] setText:[[self pizzeria] formattedPhone]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[[self pizzeria] menuUrl]];
    [[self webView] loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
