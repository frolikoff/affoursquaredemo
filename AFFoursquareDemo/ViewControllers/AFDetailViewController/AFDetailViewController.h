//
//  AFDetailViewController.h
//  AFFoursquareDemo
//
//  Created by Alex Frolikoff on 2/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFPizzeria.h"

@interface AFDetailViewController : UIViewController

@property(nonatomic, strong) AFPizzeria *pizzeria;

@end
