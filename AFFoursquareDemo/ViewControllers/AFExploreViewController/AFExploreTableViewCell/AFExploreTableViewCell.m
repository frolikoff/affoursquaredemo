//
//  AFExploreTableViewCell.m
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFExploreTableViewCell.h"
#import "AFPizzeria.h"

@interface AFExploreTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation AFExploreTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellWithPizzeria:(AFPizzeria *)pizzeria
{
    NSString *distanceString = [NSString stringWithFormat:@"%@ m", [[pizzeria distance] stringValue]];
    [[self distanceLabel] setText:distanceString];
    [[self nameLabel] setText:[pizzeria name]];
}

@end
