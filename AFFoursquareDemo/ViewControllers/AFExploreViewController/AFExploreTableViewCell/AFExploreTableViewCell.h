//
//  AFExploreTableViewCell.h
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AFPizzeria;

@interface AFExploreTableViewCell : UITableViewCell

- (void)setupCellWithPizzeria:(AFPizzeria *)pizzeria;

@end
