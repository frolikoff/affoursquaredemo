//
//  AFExploreViewController.m
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFExploreViewController.h"
#import "AFDetailViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AFDataSourceManager.h"
#import "AFExploreTableViewCell.h"

@interface AFExploreViewController () <UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic) CLLocationManager *locationManager;
@property(nonatomic) CLLocationCoordinate2D userLocation;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(nonatomic, strong) AFDataSourceManager *dsManager;

@end

@implementation AFExploreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupMapview];
    
    [self activityIndicatorStart];
    
    [self setDsManager:[[AFDataSourceManager alloc] init]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"detailVC"])
    {
        NSIndexPath *path = [self.table indexPathForSelectedRow];
        AFPizzeria *pizzeria = [[[self dsManager] getItems] objectAtIndex:path.row];
        
        AFDetailViewController *vc = [segue destinationViewController];
        [vc setPizzeria:pizzeria];
    }
}


#pragma mark - TableViewDelegate / Datasourse

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self dsManager] getItems] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@",[AFExploreTableViewCell class]];
    AFExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell)
    {
        cell = [[AFExploreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:cellIdentifier];
    }
    
    [cell setupCellWithPizzeria:[[self dsManager] getItems][indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isLastCell = indexPath.row == [[[self dsManager] getItems] count] - 1;
    BOOL isServerHasMorePizzerias = [[self dsManager] totalItemsCount] > [[[self dsManager] getItems] count];
    
    if (isLastCell && isServerHasMorePizzerias)
    {
        [self activityIndicatorStart];
        
        __weak __typeof__(self) weakSelf = self;
        
        [[self dsManager] searchPizzaNearMeNextPageWithSuccess:^(NSArray *items)
        {
            [weakSelf reloadData];
        } failure:^(NSError *error)
        {
            [weakSelf activityIndicatorStop];
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGPoint offset = [scrollView contentOffset];
    
    if (offset.y < -100)
    {
        [self refreshData];
    }
}

#pragma mark - Activity Indicator

- (void)activityIndicatorStart
{
    [[self activityIndicator] setHidden:NO];
    [[self activityIndicator] startAnimating];
}

- (void)activityIndicatorStop
{
    [[self activityIndicator] stopAnimating];
    [[self activityIndicator] setHidden:YES];
}

#pragma mark - MKMapViewDelegate

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    CLLocationCoordinate2D loc = mapView.region.center;
    _userLocation = loc;
}

#pragma mark - MapView

- (void)setupMapview
{
    [self setLocationManager:[[CLLocationManager alloc] init]];
    [[self locationManager] setDelegate:self];
    [[self locationManager] setDistanceFilter:100];
    [[self locationManager] setDesiredAccuracy:20];
    [[self locationManager] requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        _mapView.showsUserLocation = YES;
    }

    _mapView.mapType = MKMapTypeStandard;
}

- (void)addPizzerias:(NSArray *)pizzerias toMapView:(MKMapView *)mapView
{
    for (AFPizzeria *pizzeria in pizzerias)
    {
        [self addPointToMapWithPizzeria:pizzeria toMapView:mapView];
    }
}

- (void)addPointToMapWithPizzeria:(AFPizzeria *)pizzeria toMapView:(MKMapView *)mapView
{
    CLLocationCoordinate2D location2d;
    location2d.latitude = [[pizzeria latitude] floatValue];
    location2d.longitude = [[pizzeria longitude] floatValue];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location2d;
    point.title = [pizzeria name];
    point.subtitle = [pizzeria address];
    
    [mapView addAnnotation:point];
}

- (void)removePizzeriasFromMapFromMapView:(MKMapView *)mapView
{
    [mapView removeAnnotations:[mapView annotations]];
}

#pragma mark - Gestures

- (IBAction)didTapMapView:(id)sender
{
    [self refreshData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"didUpdateLocations");
    NSLog(@"%@", locations);

    CLLocation *location = [locations lastObject];
    
    CLLocationCoordinate2D location2d;
    location2d.latitude = location.coordinate.latitude;
    location2d.longitude = location.coordinate.longitude;
    _userLocation = location2d;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    region.span = span;
    region.center = location2d;
    [_mapView setRegion:region animated:YES];
    
    [self refreshData];
}

#pragma mark - Private

-(void)refreshData
{
    [self activityIndicatorStart];
    
    __weak typeof(self) weakSelf = self;
    
    [[self dsManager] refreshPizzeriasWithLocation:[self userLocation]
                                           success:^(NSArray *items)
    {
        [weakSelf reloadData];
        
    } failure:^(NSError *error)
    {
        [weakSelf activityIndicatorStop];
        NSLog(@"ERROR: %@", [error localizedDescription]);
    }];
}

- (void)reloadData
{
    [self activityIndicatorStop];
    [[self table] reloadData];
    
    [self removePizzeriasFromMapFromMapView:[self mapView]];
    NSArray *pizzerias = [[self dsManager] getItems];
    [self addPizzerias:pizzerias toMapView:[self mapView]];
}

@end
