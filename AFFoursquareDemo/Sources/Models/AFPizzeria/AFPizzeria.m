//
//  AFPizzeria.m
//  AFFoursquareDemo
//
//  Created by Admin on 07/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFPizzeria.h"

@implementation AFPizzeria

- (BOOL)isEqual:(AFPizzeria *)object
{
    if (object == self)
    {   
        return YES;
    }
    
    if (!object || ![object isKindOfClass:[self class]])
    {
        return NO;
    }
    
    if ([[self pID] isEqual:[object pID]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (NSUInteger)hash
{
    return [[self pID] hash] ^ [[self name] hash];
}

@end
