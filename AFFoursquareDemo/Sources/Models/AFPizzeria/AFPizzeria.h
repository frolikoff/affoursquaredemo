//
//  AFPizzeria.h
//  AFFoursquareDemo
//
//  Created by Admin on 07/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFPizzeria : NSObject

@property(nonatomic, strong) NSString *pID;
@property(nonatomic, strong) NSString *name;

@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSNumber *distance;
@property(nonatomic, strong) NSNumber *latitude;
@property(nonatomic, strong) NSNumber *longitude;
@property(nonatomic, strong) NSURL *menuUrl;
@property(nonatomic, strong) NSString *formattedPhone;

@end
