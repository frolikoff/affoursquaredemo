//
//  AFCDPizzeria+CoreDataProperties.h
//  
//
//  Created by Alex Frolikoff on 2/12/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AFCDPizzeria.h"

NS_ASSUME_NONNULL_BEGIN

@interface AFCDPizzeria (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSNumber *distance;
@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSNumber *longitude;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *pID;
@property (nullable, nonatomic, retain) NSString *menuUrl;
@property (nullable, nonatomic, retain) NSString *formattedPhone;

@end

NS_ASSUME_NONNULL_END
