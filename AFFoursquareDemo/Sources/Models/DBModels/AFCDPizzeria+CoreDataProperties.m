//
//  AFCDPizzeria+CoreDataProperties.m
//  
//
//  Created by Alex Frolikoff on 2/12/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AFCDPizzeria+CoreDataProperties.h"

@implementation AFCDPizzeria (CoreDataProperties)

@dynamic address;
@dynamic distance;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic pID;
@dynamic menuUrl;
@dynamic formattedPhone;

@end
