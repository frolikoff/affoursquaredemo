//
//  AFDBGateway.h
//  AFFoursquareDemo
//
//  Created by Alex Frolikoff on 09.02.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFDBGateway : NSObject

- (NSUInteger)totalItemCount;
- (void)pizzeriasWithLimit:(NSUInteger)limit offset:(NSUInteger)offset completion:(void (^)(NSArray *items, NSError *error))completion;
- (void)savePizzerias:(NSArray *)pizzerias completion:(void (^)(BOOL success, NSError *error))completion;

@end
