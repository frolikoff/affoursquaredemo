//
//  AFDBGateway.m
//  AFFoursquareDemo
//
//  Created by Alex Frolikoff on 09.02.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFDBGateway.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

#import "AFCDPizzeria.h"
#import "AFPizzeria.h"

@interface AFDBGateway()

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation AFDBGateway

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = [appDelegate managedObjectContext];
        _managedObjectModel = [appDelegate managedObjectModel];
        _persistentStoreCoordinator = [appDelegate persistentStoreCoordinator];
        
//        [self setupDataBase];
    }
    
    return self;
}

- (NSUInteger)totalItemCount
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([AFCDPizzeria class])
                                   inManagedObjectContext:[self managedObjectContext]]];
    
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    NSUInteger count = [[self managedObjectContext] countForFetchRequest:request error:&err];
    
    return count;
}

- (void)pizzeriasWithLimit:(NSUInteger)limit
                    offset:(NSUInteger)offset
                completion:(void (^)(NSArray *items, NSError *error))completion
{
    if (completion)
    {
        [[self managedObjectContext] performBlock:^
        {
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([AFCDPizzeria class])];
            NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:NSStringFromSelector(@selector(distance)) ascending:YES];
            [request setSortDescriptors:@[sort]];
            [request setFetchLimit:limit];
            [request setFetchOffset:offset];
            [request setReturnsObjectsAsFaults:NO];
            
            NSError *fetchError = nil;
            NSArray *items = [[self managedObjectContext] executeFetchRequest:request error:&fetchError];
            
            completion(items, fetchError);
       }];
    }
}

- (void)savePizzerias:(NSArray *)pizzerias completion:(void (^)(BOOL success, NSError *error))completion
{
    if ([pizzerias count] < 1)
    {
        if (completion)
        {
            completion(YES, nil);
            return;
        }
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([AFCDPizzeria class])];
    [request setReturnsObjectsAsFaults:NO];
    
    NSError *fetchError = nil;
    NSArray *items = [[self managedObjectContext] executeFetchRequest:request error:&fetchError];
    
    [pizzerias enumerateObjectsUsingBlock:^(AFPizzeria *pizzeria, NSUInteger idx, BOOL * _Nonnull stop)
    {
        __block BOOL isFound = NO;
        
        __weak typeof(self) weakSelf = self;
        
        [items enumerateObjectsUsingBlock:^(AFCDPizzeria *cdPizzeria, NSUInteger idx, BOOL * _Nonnull stop)
        {
            if ([[cdPizzeria pID] isEqualToString:[pizzeria pID]])
            {
                [weakSelf setupCDPizzeria:cdPizzeria fromPizzeria:pizzeria];
                isFound = YES;
                *stop = YES;
            }
        }];
        
        if (!isFound)
        {
            [self insertNewCDPizzeriaWithPizzeria:pizzeria];
        }
    }];
    
    NSError *saveEror = nil;
    BOOL saveSuccess = [self saveContextWithError:&saveEror];
    NSAssert(saveEror == nil, @"SAVE ERROR: %@", [saveEror localizedDescription]);
    
    if (completion)
    {
        completion(saveSuccess, saveEror);
    }
}

#pragma mark - Private

- (void)setupCDPizzeria:(AFCDPizzeria *)cdPizzeria fromPizzeria:(AFPizzeria *)pizzeria
{
    [cdPizzeria setPID:[pizzeria pID]];
    [cdPizzeria setName:[pizzeria name]];
    [cdPizzeria setAddress:[pizzeria address]];
    [cdPizzeria setDistance:[pizzeria distance]];
    [cdPizzeria setLatitude:[pizzeria latitude]];
    [cdPizzeria setLongitude:[pizzeria longitude]];
    [cdPizzeria setMenuUrl:[[pizzeria menuUrl] absoluteString]];
    [cdPizzeria setFormattedPhone:[pizzeria formattedPhone]];
}

- (void)insertNewCDPizzeriaWithPizzeria:(AFPizzeria *)pizzeria
{
    NSEntityDescription *description = [NSEntityDescription entityForName:NSStringFromClass([AFCDPizzeria class]) inManagedObjectContext:[self managedObjectContext]];
    AFCDPizzeria *cdPizzeria = (AFCDPizzeria *)[[NSManagedObject alloc] initWithEntity:description
                                       insertIntoManagedObjectContext:[self managedObjectContext]];
    
    [self setupCDPizzeria:cdPizzeria fromPizzeria:pizzeria];
    
    NSError *saveError = nil;
    [[self managedObjectContext] save:&saveError];
    
     NSAssert(saveError == nil, @"SAVE ERROR: %@", [saveError localizedDescription]);
}

- (BOOL)saveContextWithError:(NSError **)error
{
    if ([[self managedObjectContext] hasChanges])
    {
        return [[self managedObjectContext] save:error];
    }
    
    return YES;
}

@end
