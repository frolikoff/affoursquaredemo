//
//  AFParsing.m
//  AFFoursquareDemo
//
//  Created by Admin on 07/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFParsing.h"
#import "AFPizzeria.h"

@implementation AFParsing

+ (void)pizzeriasFromRsponse:(id)response completion:(void(^)(NSArray *items, NSInteger totalItemsCount))completion
{
    if (completion)
    {
        NSMutableArray *pizzerias = [NSMutableArray new];
        NSDictionary *jsonResponseDic = response[@"response"];
        NSInteger totalCount = [jsonResponseDic[@"totalResults"] integerValue];
        
        NSArray *jsonVenues = [jsonResponseDic[@"groups"] firstObject][@"items"];
        
        for (NSDictionary *venueDic in jsonVenues)
        {
            NSDictionary *pizzaDic = venueDic[@"venue"];
            
            AFPizzeria *pizzeria = [[AFPizzeria alloc] init];
            
            [pizzeria setPID:pizzaDic[@"id"]];
            [pizzeria setName:pizzaDic[@"name"]];
            
            NSDictionary *location = pizzaDic[@"location"];
            [pizzeria setAddress:location[@"address"]];
            [pizzeria setDistance:location[@"distance"]];
            [pizzeria setLongitude:location[@"lng"]];
            [pizzeria setLatitude:location[@"lat"]];
            
            [pizzeria setMenuUrl:[NSURL URLWithString:pizzaDic[@"menu"][@"mobileUrl"]]];
            [pizzeria setFormattedPhone:pizzaDic[@"contact"][@"formattedPhone"]];
            
            [pizzerias addObject:pizzeria];
        }
        
        completion(pizzerias, totalCount);
    }
}

@end
