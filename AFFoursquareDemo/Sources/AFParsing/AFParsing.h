//
//  AFParsing.h
//  AFFoursquareDemo
//
//  Created by Admin on 07/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFParsing : NSObject

+ (void)pizzeriasFromRsponse:(id)response completion:(void(^)(NSArray *items, NSInteger totalItemsCount))completion;

@end
