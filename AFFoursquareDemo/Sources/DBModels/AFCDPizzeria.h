//
//  AFCDPizzeria.h
//  
//
//  Created by Alex Frolikoff on 2/12/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AFCDPizzeria : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "AFCDPizzeria+CoreDataProperties.h"
