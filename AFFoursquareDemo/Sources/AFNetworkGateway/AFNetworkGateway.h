//
//  AFNetworkGateway.h
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

extern const NSInteger AFPageLength;

@interface AFNetworkGateway : NSObject

- (void)searchPizzeriasWithLocation:(CLLocationCoordinate2D)location
                               page:(NSNumber *)page
                            success:(void(^)(NSArray *items, NSInteger totalItemsCount))success
                            failure:(void(^)(NSError *error))failure;

- (BOOL)isReachable;

@end
