//
//  AFNetworkGateway.m
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFNetworkGateway.h"
#import <AFNetworking/AFNetworking.h>
#import <Reachability/Reachability.h>
#import "AFParsing.h"
@class AFPizzeria;

static NSString *clientID = @"CP2TQQ2QK4NUWA1CZJIGOJ5A1PCX142GONA244ETX02LG0JO";
static NSString *clientSecret = @"B2J0MDW2II2V4RYRKDL5QJ4IPYLDKIGNECD2XMHY4VBJ0WK4";

static NSString *baseURLString = @"https://api.foursquare.com/v2/";
NSInteger const AFPageLength = 10;
NSInteger const AFRadiusSearch = 10000;

@interface AFNetworkGateway()

@property(nonatomic, strong) AFHTTPSessionManager *manager;
@property(nonatomic, strong) NSDateFormatter *dateFromatter;

@end

@implementation AFNetworkGateway

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self setManager: [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString]]];
        [[self manager] setResponseSerializer: [AFJSONResponseSerializer serializer]];
        
        [self setDateFromatter: [[NSDateFormatter alloc] init]];
        [[self dateFromatter] setDateFormat:@"yyyyMMdd"];
    }
    
    return self;
}

- (void)searchPizzeriasWithLocation:(CLLocationCoordinate2D)location
                               page:(NSNumber *)page
                            success:(void(^)(NSArray *items, NSInteger totalItemsCount))success
                            failure:(void(^)(NSError *error))failure
{
    double lat = location.latitude;
    double lgt = location.longitude;
    NSString *ll = [NSString stringWithFormat:@"%f,%f", lat, lgt];
    
    NSString *dateString = [[self dateFromatter] stringFromDate:[NSDate date]];
    
    NSNumber *offset = @(AFPageLength*[page integerValue]);
    
    NSDictionary *params = @{
                             @"client_id" : clientID,
                             @"client_secret" : clientSecret,
                             @"ll" : ll,
                             @"radius" : @(AFRadiusSearch),
                             @"limit" : @(AFPageLength),
                             @"offset" : offset,
                             @"query" : @"pizza",
                             @"v" : dateString,
//                             @"intent" : @"browse"
                             };
    
    [[self manager] GET:@"venues/explore" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        if (success)
        {
            [AFParsing pizzeriasFromRsponse:responseObject completion:^(NSArray *items, NSInteger totalItemsCount)
            {
                NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                
                success([items sortedArrayUsingDescriptors:@[sort]], totalItemsCount);
            }];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"ERROR: %@", [error localizedDescription]);
        
        if (failure)
        {
            failure(error);
        }
    }];
}

- (BOOL)isReachable
{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    
    BOOL result = [reach isReachable];
    NSLog(result ? @"reach Yes" : @"reach No");
    return result;
}

@end
