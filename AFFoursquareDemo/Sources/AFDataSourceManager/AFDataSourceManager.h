//
//  AFDataSourceManager.h
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AFDataSourceManager : NSObject

- (BOOL)isLoadingNow;
- (NSArray *)getItems;

- (NSInteger)totalItemsCount;

- (void)refreshPizzeriasWithLocation:(CLLocationCoordinate2D)location
                             success:(void (^)(NSArray *items))success
                             failure:(void (^)(NSError *error))failure;

- (void)searchPizzeriasWithLocation:(CLLocationCoordinate2D)location
                               page:(NSNumber *)page
                            success:(void (^)(NSArray *items, NSInteger totalItemsCount))theSuccess
                            failure:(void (^)(NSError *error))theFailure;

- (void)searchPizzaNearMeNextPageWithSuccess:(void (^)(NSArray *items))success
                                     failure:(void (^)(NSError *error))failure;

@end
