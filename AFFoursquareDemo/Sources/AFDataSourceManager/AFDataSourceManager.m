//
//  AFDataSourceManager.m
//  AFFoursquareDemo
//
//  Created by Admin on 06/02/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AFDataSourceManager.h"
#import "AFDBGateway.h"
#import "AFNetworkGateway.h"
#import "AFPizzeria.h"

@interface AFDataSourceManager()

@property(nonatomic, strong) AFNetworkGateway *networkingGateway;
@property(nonatomic, strong) AFDBGateway *dbGateway;

@property(nonatomic, strong) NSMutableSet *items;
@property(nonatomic, assign) NSInteger totalItemsCount;

@property(nonatomic, assign) NSInteger currentPage;
@property(nonatomic, assign) CLLocationCoordinate2D currentLocation;

@property(nonatomic, assign) BOOL isLoading;

@end

@implementation AFDataSourceManager

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self setNetworkingGateway:[[AFNetworkGateway alloc] init]];
        [self setDbGateway:[[AFDBGateway alloc] init]];
        [self setItems:[NSMutableSet new]];
        [self setCurrentPage:0];
    }
    
    return self;
}

- (BOOL)isLoadingNow
{
    return [self isLoading];
}

- (NSInteger)totalItemsCount
{
    return _totalItemsCount;
}

- (NSArray *)getItems
{
    NSArray *arrayItems = [[self items] allObjects];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:NSStringFromSelector(@selector(distance)) ascending:YES];
    
    return [arrayItems sortedArrayUsingDescriptors:@[sort]];
}

- (void)refreshPizzeriasWithLocation:(CLLocationCoordinate2D)location
                             success:(void (^)(NSArray *items))success
                             failure:(void (^)(NSError *error))failure
{
    [self setCurrentLocation:location];
    
    __weak typeof(self) weakSelf = self;
    
    [self searchPizzeriasWithLocation:location page:@0 success:^(NSArray *items, NSInteger totalItemsCount)
    {
        [weakSelf setTotalItemsCount:totalItemsCount];
        [weakSelf setItems:[NSMutableSet new]];
        [[weakSelf items] addObjectsFromArray:items];
        
        if (success)
        {
            success(items);
        }
    } failure:^(NSError *error)
    {
        if (failure)
        {
            failure(error);
        }
    }];
}

- (void)searchPizzeriasWithLocation:(CLLocationCoordinate2D)location
                               page:(NSNumber *)page
                            success:(void (^)(NSArray *items, NSInteger totalItemsCount))theSuccess
                            failure:(void (^)(NSError *error))theFailure;
{
    if ([self isLoading])
    {
        return;
    }
    
    [self setIsLoading:YES];
    
    [self setCurrentLocation:location];
    
    __weak typeof(self) weakSelf = self;
    
    if ([[self networkingGateway] isReachable])
    {
        [[self networkingGateway] searchPizzeriasWithLocation:location page:page success:^(NSArray *items, NSInteger totalItemsCount)
        {
            [weakSelf setIsLoading:NO];
            [weakSelf setTotalItemsCount:totalItemsCount];
            [[weakSelf items] addObjectsFromArray:items];
            NSLog(@"items.count: %d", [[weakSelf items] count]);
            NSLog(@"totalItems: %d", [weakSelf totalItemsCount]);
            
            [[weakSelf dbGateway] savePizzerias:items completion:^(BOOL success, NSError *error)
            {
                if (theSuccess)
                {
                    theSuccess(items, totalItemsCount);
                }
            }];
            
        } failure:^(NSError *error)
        {
            [weakSelf setIsLoading:NO];
            
            if (theFailure)
            {
                theFailure(error);
            }
        }];
    }
    else
    {
        __weak typeof(self) weakSelf = self;
        
        [[self dbGateway] pizzeriasWithLimit:AFPageLength offset:[page integerValue]*10 completion:^(NSArray *items, NSError *error)
        {
            NSInteger totalCount = [[weakSelf dbGateway] totalItemCount];
            [weakSelf setTotalItemsCount:totalCount];
            
            if (!error)
            {
                if (theSuccess)
                {
                    theSuccess(items, totalCount);
                }
            }
            else
            {
                if (theFailure)
                {
                    theFailure(error);
                }
            }
        }];
    }
}

- (void)searchPizzaNearMeNextPageWithSuccess:(void (^)(NSArray *items))theSuccess
                                     failure:(void (^)(NSError *error))theFailure
{
    NSNumber *nextPage = @(_currentPage + 1);
    NSLog(@"nextPage: %@", nextPage);
    
    __weak __typeof(self)weakSelf = self;
    
    if ([[self networkingGateway] isReachable])
    {
        [self searchPizzeriasWithLocation:[self currentLocation] page:nextPage success:^(NSArray *items, NSInteger totalItemsCount)
        {
            [weakSelf setCurrentPage:[nextPage integerValue]];
            
             if (theSuccess)
             {
                 theSuccess(items);
             }
         } failure:^(NSError *error)
         {
             if (theFailure)
             {
                 theFailure(error);
             }
         }];
    }
    else
    {
        NSUInteger totalItems = [[self dbGateway] totalItemCount];
        NSInteger wantedItemsCount = [nextPage integerValue]*AFPageLength;
        
        if (totalItems <= wantedItemsCount)
        {
            if (theSuccess)
            {
                theSuccess(@[]);
            }
            
            return;
        }
        
        __weak typeof(self) weakSelf = self;
        
        [[self dbGateway] pizzeriasWithLimit:AFPageLength offset:wantedItemsCount completion:^(NSArray *items, NSError *error)
         {
             [weakSelf setTotalItemsCount:[[weakSelf dbGateway] totalItemCount]];
             [[weakSelf items] addObjectsFromArray:items];
             
             if (!error)
             {
                 if (theSuccess)
                 {
                     theSuccess(items);
                 }
             }
             else
             {
                 if (theFailure)
                 {
                     theFailure(error);
                 }
             }
         }];
    }
}

@end
